<?php

declare(strict_types=1);

namespace Drupal\Tests\expirable_content\Kernel\Plugin\Field;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\expirable_content\Entity\ExpirableContentType;
use Drupal\node\Entity\Node;
use Drupal\Tests\field\Kernel\FieldKernelTestBase;

/**
 * Tests the functionality of the ExpirableContentFieldItemList.
 *
 * @covers ExpirableContentFieldItemList
 * @group expirable_content
 */
class ExpirableContentFieldItemListTest extends FieldKernelTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'user',
    'system',
    'field',
    'node',
    'expirable_content'
  ];

  /**
   * The current timezone defined by Drupal. Defaults to 'America/New_York'.
   *
   * @var \DateTimeZone
   */
  public \DateTimeZone $timezone;

  protected function setUp(): void {
    parent::setUp();
    $tz = \Drupal::config('system.date')->get('timezone.default') ?: 'America/New_York';
    $this->timezone = new \DateTimeZone($tz);
    $this->installEntitySchema('node');
    $this->installEntitySchema('user');
    $this->installEntitySchema('expirable_content');
    $this->installEntitySchema('expirable_content_type');
  }

  /**
   * Tests computation of expiration dates.
   */
  public function testComputeExpireDate() {

    // Setup the Expirable Content Type.
    $expirableContentType = ExpirableContentType::create([
      'status' => TRUE,
      'bundle' => 'node.article',
      'id' => 'node.article',
      'field' => 'changed',
      'days' => 7,
      'warn' => 3,
      'entity_type' => 'node',
      'entity_bundle' => 'article',
    ]);
    $expirableContentType->save();

    // This is needed until we implement automatic cache invalidation.
    \Drupal::cache()->deleteAll();

    // Create a node entity with required fields.
    $node = Node::create([
      'type' => 'article',
      'title' => 'Test Node',
    ]);
    $node->save();

    // Setup expected warning and expiration dates.
    $days = $expirableContentType->days();
    $warn = $expirableContentType->warn();
    $expDateTime = $node->expiration_date->value;
    $warDateTime = $node->warning_date->value;
    $today = DrupalDateTime::createFromTimestamp(strtotime('now'), $this->timezone);
    $today->setTime(hour: 0, minute: 0, second: 0);
    $expInterval = new \DateInterval('P' . $days . 'D');
    $warnInterval = new \DateInterval('P' . $warn . 'D');
    $expectedDate = DrupalDateTime::createFromTimestamp($today->getTimestamp(), $today->getTimezone());
    $expectedDate->add($expInterval);

    // Assert expiration date is correct.
    self::assertSame($expDateTime, $expectedDate->getTimestamp());
    // Assert warning date is correct.
    $expectedDate->sub($warnInterval);
    self::assertSame($warDateTime, $expectedDate->getTimestamp());
  }
}
