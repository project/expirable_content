## INTRODUCTION

Configure any content entity to have an expiration and warning date.

This module is inspired by [Node Auto Expire](https://www.drupal.org/project/node_auto_expire) and Core's Content Moderation module.

### Differences with Node Auto Expire
Node Auto Expire only works with Nodes, and assumes an email is the desired action to take for warning and expiration dates. Expirable Content doesn't take any action when the expiration or warning dates arrive. This allows for leveraging rules-based frameworks like Rules, ECA, or the Message module suite, etc.

## INSTALLATION

Install as you would normally install a contributed Drupal module.
See: https://www.drupal.org/node/895232 for further information.

## CONFIGURATION

None.

## MAINTAINERS

Current maintainers for Drupal 10:

- Daniel Sasser (coderdan) - https://www.drupal.org/u/coderdan
