# Expirable Content
A Drupal module that allow content entities to be expirable.

### Todo
- [ ] Ensure dependencies are exported when creating an expirable content type
- [ ] Write tests
- [ ] Implement a way to seed the initial expirable content entities without having to load + save them
-

### Completed ✓
- [x] Create TODO.md
