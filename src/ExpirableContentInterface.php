<?php

declare(strict_types = 1);

namespace Drupal\expirable_content;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface defining an expirable content entity type.
 */
interface ExpirableContentInterface extends ContentEntityInterface {

}
